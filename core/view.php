<?php

class View {

    protected $variables = array();
    protected $_controller;
    protected $_action;

    function __construct($module, $subModule, $controller, $action) {
        $this->_module = $module;
        $this->_subModule = $subModule;
        $this->_controller = $controller;
        $this->_action = $action;
        $this->_dir = APPLICATION_DIR . 'module/' . $module . '/' . $subModule;
    }

    /** Set Variables * */
    function set($name, $value) {
        $this->variables[$name] = $value;
    }

    /** Display Template * */
    function render() {
        extract($this->variables);
        if ($this->_module == 'api') {
            return;
        }

        include_once($this->_dir . '/template/' . $this->_action . ".php");
    }

}
