<?php

$here = dirname(__FILE__) . "/..";
$here = ($h = realpath($here)) ? $h : $here;
define('ROOT_DIR', str_replace('\\', '/', $here . '/'));
unset($here);
unset($h);        
define('DEVELOPMENT_ENVIRONMENT', false);

define('CORE_DIR', ROOT_DIR . 'core/');
define('CONFIG_DIR', ROOT_DIR . 'config/');
define('APPLICATION_DIR', ROOT_DIR . 'application/');


/**
 * Autoload functionality
 */
function globalAutoloader($classname) {

    $classpath = array('helper/libraries', 'helper/model');

    foreach ($classpath as $path) {
        $filename = ROOT_DIR . $path . "/class." . lcfirst($classname) . ".php";
        if (file_exists($filename)) {
            include_once($filename);
            return;
        }
    }
}
spl_autoload_register('globalAutoloader');
