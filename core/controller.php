<?php

class Controller {

    protected $_module;
    protected $_action;
    protected $_template;
    protected $_templateName;

    function __construct($module, $subModule, $controller, $action) {
        $this->_module = $module;
        $this->_subModule = $subModule;
        $this->_controller = $controller;
        $this->_action = $action;
        
        $this->_template = new View($module, $subModule, $controller, $action);
    }

    function setTemplate($value) {
        $this->_template->setView($value);
    }

    function set($name, $value) {
        $this->_template->set($name, $value);
    }

    function __destruct() {
        $this->_template->render();
    }

}
