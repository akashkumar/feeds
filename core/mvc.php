<?php

/**
 * File to handle basic mvc functionality like handling controller MODEL VIEW INTERACTIONS
 */
function setReporting() {
    if (DEVELOPMENT_ENVIRONMENT == true) {
        error_reporting(E_ERROR);
        ini_set('display_errors', 0);
    }else{
        error_reporting(E_ERROR);
        ini_set('display_errors', 0);
    }
}

/** Main Call Function * */
function callHook($url) {
    global $route;
    $route = Router::getRoute($url);

    $module = $route["module"];
    $controller = ucfirst($route["controller"]) . 'Controller';
    $action = $route["action"];

    $controllerName = $route["sub_module"];
    
    $dispatch = new $controller($module, $controllerName, $controller, $action);
    
    if ((int) method_exists($controller, $action)) {
        call_user_func_array(array($dispatch, $action), array($url));
    } else {
        
    }
}

function applicationAutoloading($classname) {
    global $route;

    $module = APPLICATION_DIR . "module/" . $route["module"] . "/" . $route["sub_module"];
    if (count($route) == 4) {
        $classpath[] = $module . "/controller". "/" . $classname . ".php";
    }
    
    if(stristr($classname,'_')){
        $classNameArr = explode("_",$classname);
        $module = (count($classNameArr)>2)?$classNameArr[0]:"frontend";
        $controller = (count($classNameArr)>2)?$classNameArr[1]:$classNameArr[0];
        $classpath[] = APPLICATION_DIR . "module/" . $module . "/" . $controller."/model/".$classname . ".php";
    }
    foreach ($classpath as $filename) {
        
        if (file_exists($filename)) {
            include_once($filename);
            return;
        }
    }
}

spl_autoload_register('applicationAutoloading');
setReporting();
callHook($_GET["url"]);

