<?php

/**
 * Manage all session varials on home at one palce
 */

class home_session extends Session {

    public static function setSessionVariables() {

        //FROM GET PARAMS
        Session::set('source', $_GET['source']);
        Session::set('email', $_GET['email']);
        $quincena_date = Quincena::getQuincenaDate();
        Session::set('quincena_date', $quincena_date, null);
        if (isset($_SESSION['quincena_date'])) {
            $specialPromo = 'quincena';
        }
        Session::reset('promocode', $_GET['promocode'], $specialPromo);
        Session::reset('referralcode', $_GET['referralcode']);
        Session::set('HTTP_REFERER', $_SERVER['HTTP_REFERER'], 'Direct');
        Session::reset('utm_campaign', $_GET['utm_campaign'], $specialPromo);
        Session::set('source', $_GET['utm_source'], 'Direct');

        $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        Session::set('ActualLink', $actual_link);

        if (empty($_SESSION['userID'])) {
            if (!empty($_COOKIE['userID'])) {
                $_SESSION['userEmail'] = $_COOKIE['userEmail'];
                $_SESSION['userID'] = $_COOKIE['userID'];
            } else {
                unset($_SESSION['userEmail']);
            }
        }

        $unsetArr = array('amount', 'finalamount', 'mobile', 'operator', 'cashback');
        Session::erase($unsetArr);

        //FROM DBS
        Session::set('Televia', $_COOKIE['Televia']);
        Session::set('Viapass', $_COOKIE['Viapass']);
        Session::set('PaseUrbano', $_COOKIE['PaseUrbano']);
        Session::set('meta','na');
        if (!empty($_SESSION['userID'])) {
            $fields = array('Televia', ' PaseUrbano', ' Viapass', 'trust_score', 'otpRequest', 'otpRequestTime');
            $filter = array('id' => $_SESSION['userID'], 'status' => 1);
            $result = TblUsers::getData('tbl_users', $filter, $fields);
            foreach ($result as $rowcd) {
                Session::set('Televia', $rowcd['Televia']);
                Session::set('Viapass', $rowcd['Viapass']);
                Session::set('PaseUrbano', $rowcd['PaseUrbano']);
                Session::set('meta', $rowcd['trust_score']);
                Session::set('otpRequest', $rowcd['otpRequest']);
                Session::set('otpRequestTime', $rowcd['otpRequestTime']);
            }
        }
    }

}
