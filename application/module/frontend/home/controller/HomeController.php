<?php

/**
 * Class to Control Home page requests
 */
class HomeController extends Controller {

    /**
     * Function to handle action on show page on Home
     * @method GET
     * @param limit (optional)
     */
    public function show($data) {
        $limit = ($_GET["limit"] > 0) ? $_GET["limit"] : 100;

        $feedArr = FeedsTable::objects()->filter(array("status" => 1))->order_by('updated')
                        ->values('id', 'title', 'summary', 'link', 'updated', 'source__feedId')->limit($limit)->all();

        $this->set('feedArr', $feedArr);
    }

    /**
     * Function to handle add action page on Home
     */
    public function add($data) {
        $feedUrls = FeedSourceTable::objects()->filter(array("status" => 1))->values()->all();

        $this->set('feedUrl', $feedUrls);
    }

}
