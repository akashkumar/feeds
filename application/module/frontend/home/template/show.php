<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://ogp.me/ns/fb#">
    <head>
        <style>
            body{
                background: #fff;
            }
            .feed{
                max-width:600px;
                width: 60%;
                background: #fff;
                box-shadow: 0px 0px 10px 1px #666;
                padding: 20px;
                margin:0 auto;
                margin-bottom: 10px;
            }
            .content{
                margin-bottom: 10px;
            }
            .title{
                font-weight: bolder;
                text-decoration: underline;
            }
            .date,.source{
                font-size: 9px;
                color: #666;

            }
            .date{


            }
            .source{
                float:right;

            }
            .url{
                color: #00f;
                font-size: 10px;
            }
            img{
                    max-width: 100%;
            }
        </style>
    </head>
    <body>
        <div class='feed-container'>
            <?php foreach ($feedArr as $feed) { ?>
                <div class='feed'>
                    <div class='title'><?php echo $feed["title"]; ?></div>

                    <div class='content'><?php echo $feed["summary"]; ?></div>
                    <div class='url'><?php echo $feed["link"]; ?></div>
                    <div>
                        <div class='source'> <?php echo $feed["source__feedId"]; ?> </div>
                        <div class='date'> <?php echo $feed["updated"]; ?> </div>
                    </div>
                </div>
            <?php } ?>

        </div>
    </body>
</html>
