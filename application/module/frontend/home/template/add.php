<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://ogp.me/ns/fb#">
    <head>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <style>
            .feed-container,.feed-url-container{
                width: 40%;
                padding:30px;
                box-shadow: 0px 1px 10px 1px #666;
                margin:0 auto;
                margin-top:10%;
            }
            .feed-url-container{
                margin-top:10px;
            }
            .feed-url-container table,tr{
                width:100%;
            }
            .feed-url-container td{
                max-width:30%;
                padding:5px;
            }
            .feed-url-container td .fetch{
                background:blue;
                color:#fff;
                padding:3px;
                cursor: pointer;
            }
            .feed-url-container td .url{
                color:#666;
                font-size: 10px;
            }
            .feed-container input[type='text']{
                width:60%;
            }
            input{
                height:30px;
            }
        </style>
    </head>
    <body>
        <div class='feed-container'>
            <input name="feed-url" type="text" placeholder="Feed Url"/>
            <input class="submit" type="submit" value="Add Feed Url" />

        </div>
        <div class='feed-url-container'>
            <table>
                <?php foreach ($feedUrl as $source) { ?>
                    <tr>
                        <td><?php echo $source["title"]; ?><br><span class='url'><?php echo $source["url"]; ?></span></td>
                        <td><?php echo ($source["updated"] ?: "Never"); ?></td>
                        <td><span class='fetch' feed_id='<?php echo $source["id"]; ?>'>Fetch</span></td>
                    </tr>
                <?php } ?>
            </table>

        </div>        
        <script>

            $('.submit').on('click', function () {
                var feedUrl = $('input[name=feed-url]').val();
                if (feedUrl.length < 7) {
                    alert("Please check URL, seems to be incorrect!");
                    return false;
                }
                $.ajax({
                    url: 'api/v1/add/url',
                    data: {feedUrl: feedUrl},
                    dataType: 'json',
                    type: 'POST',
                    success: function (response) {
                        console.log(response);
                        if (response.status == "success") {
                            alert("Success - " + response.message);
                            window.location.reload();
                        } else
                            alert(response.message);
                    }, error: function (e) {
                        alert("Error - Something Went Wrong!!!");
                    }
                });
            });
            $('.fetch').on('click', function () {
                var feedId = $(this).attr('feed_id');

                $.ajax({
                    url: 'api/v1/fetch/feeds',
                    data: {id: feedId},
                    dataType: 'json',
                    type: 'GET',
                    success: function (response) {
                        console.log(response);
                        if (response.status == "success") {
                            alert("Success - " + response.message);
                            window.location.reload();
                        } else
                            alert(response.message);
                    }, error: function (e) {
                        alert("Error - Something Went Wrong!!!");
                    }
                });
            });
        </script>
    </body>
</html>
