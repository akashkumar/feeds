<?php

/*
 * Class to Controll Feed APIs
 */

class FeedController extends Controller {

    /**
     * Function to add feed url
     * @method POST
     * @param feedUrl with http,https complete url
     * @return response
     */
    public function addurl($data) {
        $param = $_POST;
        Validator::checkPost();

        $feed = new Feed();
        $feed->AddNewFeedUrl($_POST["feedUrl"]);

        $done = array("status" => "success", "message" => "Feed Url Added");
        ResponseHandler::respond($done);
    }

    /**
     * Function to fetch data from feed id
     * @method GET
     * @param id - valid feed id
     * @return response
     */
    public function fetch($data) {
        $param = $_GET;
        Validator::checkGet();

        $feed = new Feed();
        $feed->fetchFeeds($param["id"]);

        $done = array("status" => "success", "message" => "Feed Fetched SuccessFully");
        ResponseHandler::respond($done);
    }

}
