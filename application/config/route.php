<?php

/**
 * Class to handle routing
 */
class Router {

    public static $routes = array(
        "" => array("frontend", "home", "home", "index"),
        "show" => array("frontend", "home", "home", "show"),
        "add" => array("frontend", "home", "home", "add"),
        "api/v1/add/url" => array("api", "v1", "feed", "addurl"),
        "api/v1/fetch/feeds" => array("api", "v1", "feed", "fetch"),
        "404" => array("frontend", "error", "error", "404")
    );
    public static $keys = array("module", "sub_module", "controller", "action");

    public static function getRoute($url) {
        global $route;
        $route = self::$routes[trim($url, '/')];
        if (!empty($route)) {
            $route = array_combine(self::$keys, $route);
        }

        if (empty($route)) {
            echo "404";
            die;
        }

        return $route;
    }

}
