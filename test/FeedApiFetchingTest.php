<?php

include_once('core/library.php');
require ROOT_DIR.'vendor/autoload.php';
//Model
include(CORE_DIR . 'mysqli.php');
include(CONFIG_DIR . 'db_connection.php');
include(CORE_DIR . 'class.orm.php');

use \PHPUnit_Framework_TestCase;

class FeedApiFetchingTest extends PHPUnit_Framework_TestCase {

    public function testFeedApiToFetchIsValid() {
        
        /*
         * Using Guzzle to make API requests
         */
        $client = new \GuzzleHttp\Client();
        
        $request = $client->request('GET', 'http://localhost/framework/trivago/api/v1/fetch/feeds?id=4');
        $response = $request->getStatusCode();

        $this->assertEquals(200, $response); //API response Code Success
    }

}
?>