<?php

include_once('core/library.php');

//Model
include(CORE_DIR.'mysqli.php');
include(CONFIG_DIR.'db_connection.php');
include(CORE_DIR.'class.orm.php');

use \PHPUnit_Framework_TestCase;

class FeedLoadTest extends PHPUnit_Framework_TestCase
{
  public function testLoadIsValid()
  {

    $connObj = new Feed('http://rss.dw.com/atom/rss-en-all');
    $output = $connObj->LoadFeedUrl();
    
    $this->assertInstanceOf('SimpleXMLElement',$output);
    
  }
}
?>