<?php
/*
 * Class to Test for Feed Url Validator Function
 */
include_once('core/library.php');

//Model
include(CORE_DIR.'mysqli.php');
include(CONFIG_DIR.'db_connection.php');
include(CORE_DIR.'class.orm.php');

use \PHPUnit_Framework_TestCase;
class FeedValidityTest extends PHPUnit_Framework_TestCase
{
  public function testFeedUrlIsValid()
  {
    $feedObj = new Feed();
    $valid = $feedObj->checkValidFeedUrl('http://rss.dw.com/atom/rss-en-all');
    
    $this->assertEquals(1,$valid);
  }
}
?>