<?php

/*
 * Model classes of tables
 */
class ParkingTable extends VerySimpleModel{
    static $meta = array(
            'table' => PARKING_TABLE,
            'pk' => array('parking_id')
    );
}
