<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class FeedsTable extends VerySimpleModel {

    static $meta = array(
        'table' => FEEDS_TABLE,
        'pk' => array('id'),
        'joins' => array(
            'source' => array(
                'constraint' => array('feed_source_id' => 'FeedSourceTable.id')
            ),
        )
    );

}
