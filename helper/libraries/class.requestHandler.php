<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class requestHandler{
    public static function handleRequestURI() {
        if (isset($_SERVER['HTTP_REFERER']) && strpos(strtolower($_SERVER['HTTP_REFERER']), 'santander') !== false) {
            header("Location: indexs.php");
            die();
        }

        $isFailedPages = 0;
        if (strpos(strtolower($_SERVER['REQUEST_URI']), 'noexito') !== false) {
            $currentpage = 'noexito desktop';
            $isFailedPages = 5;
            $reason = 'failed noexito';
        } else if (strpos(strtolower($_SERVER['REQUEST_URI']), 'unsuccess') !== false) {
            $currentpage = 'unsuccess desktop';
            $isFailedPages = 4;
            $reason = 'failed unsuccess';
        } else if (strpos(strtolower($_SERVER['REQUEST_URI']), 'gclid') !== false) {
            $currentpage = 'home sem desktop';
        } else {
            $currentpage = 'home general desktop';
        }
        return array("currentpage" => $currentpage, "isFailedPages" => $isFailedPages, "reason" => $reason);
    }
}