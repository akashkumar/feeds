<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Validator {

    public static function checkPost() {
        if (!$_POST) {
            $error = array("status" => "error", "message" => "invalid request");
            ResponseHandler::respond($error);
        }
    }

    public static function checkGet() {
        if (!$_GET) {
            $error = array("status" => "error", "message" => "invalid request");
            ResponseHandler::respond($error);
        }
    }

}
