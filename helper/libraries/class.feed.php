<?php

/*
 * Class to handle Feed
 * 
 */

class Feed {

    private $__feedUrl;
    private $__feedMeta;

    public function __construct($url = '') {
        if ($url)
            $this->__feedUrl = $url;
    }

    /**
     * Function to add Feed Url to database, handles validity and duplicacy checks
     * @param $url URL of feed with http,https
     * @return 
     */
    public function addNewFeedUrl($url) {
        $this->feedUrl = $url;

        $this->checkValidFeedUrl($_POST["feedUrl"]);
        $this->checkDuplicateFeedMeta();
        try {
            $feedTableObj = FeedSourceTable::create();
            $feedTableObj->title = $this->__feedMeta["title"];
            $feedTableObj->feedId = $this->__feedMeta["id"];
            $feedTableObj->url = $this->__feedUrl;
            $feedTableObj->save();
        } catch (Exception $e) {
            $error = array("status" => "error", "message" => "Exception Occured");
            ResponseHandler::respond($error);
        }
    }

    /**
     * Function to Load Feed URL and return parsed xml
     * @param 
     * @return parse XML
     */
    public function LoadFeedUrl() {
        $xmlUrl = $this->__feedUrl;
        $xmlRaw = file_get_contents($xmlUrl);

        if (empty($xmlRaw)) {
            $error = array("status" => "error", "message" => "Feed content is Empty");
            ResponseHandler::respond($error);
        }

        try {
            $xmlRaw = str_replace("]]>", '', str_replace("<![CDATA[", '', $xmlRaw));
            $xml = simplexml_load_string($xmlRaw);
            if (!$xml) {
                $error = array("status" => "error", "message" => "Feed content is Not a valid XML");
                ResponseHandler::respond($error);
            }
        } catch (Exception $e) {
            $error = array("status" => "error", "message" => "Exception While parsing feed");
            ResponseHandler::respond($error);
        }

        return $xml;
    }

    /**
     * Function to getMetaInfo from URL
     * @param $xml Parse Xml
     * @return $meta array of meto information
     */
    private function getMetaInfo($xml) {
        $xmlMetaArr = json_decode(json_encode($xml), true);
        $xmlMetaArr = $xmlMetaArr;
        $meta["title"] = $xmlMetaArr["title"];
        $meta["id"] = $xmlMetaArr["id"];
        $meta["updated"] = $xmlMetaArr["updated"];

        if (empty($meta["updated"]) || empty($meta["id"]) || empty($meta["title"])) {
            $error = array("status" => "error", "message" => "Not all required meta info present in feed", "extra" => json_encode($meta));
            ResponseHandler::respond($error);
        }
        return $meta;
    }

    /**
     * Function accepts Url and check if its valid or not
     * @param type $url (Optional, if passed in constructor)
     * @return int
     */
    public function checkValidFeedUrl($url = '') {
        if ($url)
            $this->__feedUrl = $url;

        $xmlParsedFeed = $this->LoadFeedUrl();
        $this->__feedMeta = $this->getMetaInfo($xmlParsedFeed);
        return 1;
    }

    /**
     * Function to check if similar Feed Exists or not
     * @return
     */
    private function checkDuplicateFeedMeta() {
        $countOfFeedMeta = FeedSourceTable::objects()->filter(array("title" => $this->__feedMeta["title"], "url" => $this->__feedUrl, "feedId" => $this->__feedMeta["id"], "status" => 1))->count();
        if ($countOfFeedMeta > 0) {
            $error = array("status" => "error", "message" => "Feed Already in Database");
            ResponseHandler::respond($error);
        }
    }

    /**
     * Function accepts fetch entries from xml containing feeds
     * @param $xml Parsed Xml
     * @return $entries array
     */
    public function getFeedEntries($xml) {
        $xmlEntryArr = json_decode(json_encode($xml), true);
        $xmlEntryArr = $xmlEntryArr["entry"];

        return $xmlEntryArr;
    }

    /**
     * Function to fetch feed entries and insert in db
     * @param $id (int) feed Id
     * @return 
     */
    public function fetchFeeds($id) {
        if ($id<1) {
            $error = array("status" => "error", "message" => "Feed Id not specified");
            ResponseHandler::respond($error);
        }
        $feedSourceObj = FeedSourceTable::objects()->filter(array("id"=>$id))->first();
        if (!$feedSourceObj) {
            $error = array("status" => "error", "message" => "Invalid Feed Url Id");
            ResponseHandler::respond($error);
        }

        $this->__feedUrl = $feedSourceObj->url;

        $xmlParsedFeed = $this->LoadFeedUrl();
        $this->__feedMeta = $this->getMetaInfo($xmlParsedFeed);
        if ($feedSourceObj->updated && strtotime($feedSourceObj->updated) >= strtotime($this->__feedMeta["updated"])) {
            $error = array("status" => "error", "message" => "Latest Feed Already Available from This Url");
            ResponseHandler::respond($error);
        }
        $entries = $this->getFeedEntries($xmlParsedFeed);
        $count = 0;
        foreach ($entries as $k => $entry) {
            if (strtotime($entry["updated"]) > strtotime($feedSourceObj->updated)) {
                $feedsObj = FeedsTable::create();
                $feedsObj->title = $entry["title"];
                $feedsObj->feed_content_id = $entry["id"];
                $feedsObj->summary = $entry["summary"];
                $feedsObj->link = $entry["@attributes"]["href"];
                $feedsObj->updated = $entry["updated"];
                $feedsObj->feed_source_id = $id;
                $feedsObj->save();
                $count++;
            }
        }
        $feedSourceObj->updated = date('Y-m-d H:i:s', strtotime($this->__feedMeta["updated"]));
        $feedSourceObj->save();

        $success = array("status" => "success", "message" => $count . " - Feeds Fetched and added to database");
        ResponseHandler::respond($success);
    }

}
